var loadImg = function (event) {
  var image = document.getElementById("output");
  image.src = URL.createObjectURL(event.target.files[0]);
  // Store
  //localStorage.profile = image.src;
};

function Edit() {
  var hide = document.getElementById("visual");
  var edit = document.getElementById("edit button");
  var save = document.getElementById("save button");
  var form = document.getElementById("edit form");

  if (form.style.display == 'block') {
    hide.style.display = 'none';
  }
  if (hide.style.display == 'none') {
    hide.style.display = 'block';
  }
  else {
    hide.style.display = 'none';
    edit.style.display = 'none';
    form.style.display = 'block';
    save.style.display = 'block';
  }
}

function popUp() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}