from rest_framework import serializers
from basicApp.models import User, Scooter, BookingEntity, Feedback

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model= User
        fields = (
            'UserID',
            'FirstName',
            'LastName',
            'Email',
            'Password',
            'UserType')

class ScooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scooter
        fields = (
            'ScooterID',
            'Location',
            'Available',
            )

class BookingEntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = BookingEntity
        fields = (
            'BookingReference',
            'UserID',
            'ScooterID',
            'StartTime',
            'EndTime',
            'PricePaid',
            )

class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = (
            'FeedbackID',
            'UserID',
            'BookingReference',
            'Rating',
            'Text',
            'Priority'
            )