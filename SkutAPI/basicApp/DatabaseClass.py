from getpass import getuser
from re import A
from basicApp import views
from basicApp.models import User, BookingEntity, Feedback, Scooter
from datetime import datetime, timedelta
import pytz
from django.core.mail import send_mail






# testing if username exists
def userTest(email):
    for Users in User.objects.all():
        if email in Users.Email:
            return 1
    else:
        print("user does not exist")
        return 0


# testing if pass matches username
def passTest(email, password):

    if password in User.objects.filter(Email=email).Password:
        return 1
    else:
        print("incorrect password")
        return 0

def changePassword(current_user, newpass):
    ID =  getIDByEmail(current_user)
    print("Id =")
    print(ID)
    print(newpass)
    users = User.objects.all()
    for user in users:
        if user.UserID == ID:
            user.Password = newpass
            user.save()
            print(user.Password)
            print("User pass changed")
            return 1
            
# registration with new username and pass
def regUserTest(firstName, surname, email, confirmEmail, password):

    # check for valid fields
    if not email or not confirmEmail or not password or not firstName or not surname:
        print("missing fields")
        return 0
    if confirmEmail != email:
        print("email not matching")
        return 0
    if '@' not in email or '.' not in email:
        print("invalid email address")
        return 0

    # check if user is taken
    for Users in User.objects.all():
        if email in Users.Email:
            print("email already registered!")
            return 0

    # check if password is valid
    passValid = 1
    if len(password) < 6:
        print('password cannot be less than 6 characters') # TODO: make error messages pass back to HTML
        passValid = 0

    if not any(char.isdigit() for char in password):
        print('password should have at least one number')
        passValid = 0

    if not any(char.isupper() for char in password):
        print('password should have at least one uppercase letter')
        passValid = 0

    if not any(char.islower() for char in password):
        print('password should have at least one lowercase letter')
        passValid = 0

    return passValid


def connect():
    print("connect to database")


# validates user info and returns user type
def login(userData): # 1 = Customer, 2 = Employee, 3 = Manager
    print("login")

    email = userData.get('email')
    password = userData.get('password')

    if userTest(email):
        if passTest(email,password):
            user = getUserByID(getIDByEmail(email))
            if user.UserType == 0:
                return 1
            if user.UserType == 1:
                return 2
            if user.UserType == 2:
                return 3
    else:
        return 0


# sets up new users from sign up page
def register(userData): # 1 = Registered
    print("register")

    firstName = userData.get('fname')
    surname = userData.get('lname')
    email = userData.get('email')
    confirmEmail = userData.get('confemail')
    password = userData.get('password')
    discount = userData.get('status') #0= None, 1= Student, 2= Senior

    # tests user fields and adds to database if valid
    if regUserTest(firstName, surname, email, confirmEmail, password):
        sendAccDetails(firstName, surname, email, password)
        newUser = User(FirstName=firstName, LastName=surname, Email=email, Password=password, UserType=0, DiscountStatus=discount)
        newUser.save()
        return 1
        
        
    else:
        return 0


# checks for credit card syntax
def cardCheck(cardNumber, expiryMMYY, CVV):
    if len(cardNumber) != 16 or not cardNumber.isnumeric():
        print("invalid card number")
        return 0
    if len(expiryMMYY) != 4 or int(expiryMMYY) > 1299 or int(expiryMMYY) < 0 or not expiryMMYY.isnumeric():  # TODO: better date validation
        print("invalid expiry date!")
        return 0
    if len(CVV) != 3 or not CVV.isnumeric():
        print("invalid CVV")
        return 0
    else:
        return 1


def getIDByEmail(email):
    users = User.objects.all()
    for user in users:
        if user.Email == email:
            return user.UserID
    return -1


def getUserByID(ID):
    users = User.objects.all()
    for user in users:
        if user.UserID == ID:
            return user
    return 0


def getBookingByReference(ref):
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if int(booking.BookingReference) == int(ref):
            return booking
    return -1


def getScooterByID(ID):
    scooters = Scooter.objects.all()
    for scooter in scooters:
        if scooter.ScooterID == ID:
            return scooter


def getScooterByBooking(ID):
    for booking in BookingEntity.objects.all():
        if booking.BookingReference == ID:
            return booking.ScooterID


def updateScooters():
    for booking in BookingEntity.objects.all():
        if booking.EndTime < pytz.timezone("Europe/London").localize(datetime.now()):
            scooter = booking.ScooterID
            if scooter.Available == False:
                scooter.Available = True
                scooter.save()
                print(booking.BookingReference)
                print("Booking ended")


def leaveFeedback(email, rating, text, bookingReference):
    if not bookingReference.isnumeric():
        print("Invalid booking reference!")
        return 0
    feedback = Feedback()
    referencedBooking = getBookingByReference(bookingReference)
    if referencedBooking == -1:
        print("Invalid booking!")
        return 0
    else:
        feedback.BookingReference = referencedBooking
    if not getUserByID(getIDByEmail(email)):
        print("Invalid email!")
        return -1
    feedback.UserID = getUserByID(getIDByEmail(email))
    feedback.Rating = rating
    feedback.Text = text
    feedback.Priority = 0
    feedback.save()
    print("Saved!")
    return 1



def change2ndPriorityto1():
    
    feedbacks = Feedback.objects.all()
    i=1
    for feedback in feedbacks:
        if i == 2:
            feedback.Priority = 1
    return 0


def getAvailableScooter(location):
    scooters = Scooter.objects.all()
    for scooter in scooters:
        if scooter.Location == location and scooter.Available == True:
            return scooter
    return 0

def ScooterCount(location):
    i = 0
    scooters = Scooter.objects.all()
    for scooter in scooters:
        if scooter.Location == location and scooter.Available == 1:
            i = i + 1
    return i

global receiptarray 

receiptarray = {'loggedIn':False,
            'email': "",
            'bookingR': "",
            'starttime': "",
            'endtime': "",
            'price': "",
            'scooterID': "",
            'bookingRef':"",
            'fname':"",
            'lname':""
            }


def saveCard(email, cardname, cardnum, cardexp):
    user = getUserByID(getIDByEmail(email))

    user.CardName = cardname
    user.CardNum = cardnum
    user.CardExp = cardexp
    user.save()

def makeBooking(email, duration, location): #duration is in hours
    global RATE
    for user in User.objects.all():
            if user.FirstName == "Manny":
                RATE = user.CardNum

    if getAvailableScooter(location) == 0:
        print("No available scooters!")
        return 0

    user = getUserByID(getIDByEmail(email))
    booking = BookingEntity()
    booking.UserID = user
    booking.ScooterID = getAvailableScooter(location)
    booking.StartTime = pytz.timezone("Europe/London").localize(datetime.now())
    print(booking.StartTime)
    booking.EndTime = booking.StartTime + timedelta(hours=duration)
    booking.PricePaid = RATE * discountUser(user) * duration

    booking.save()

    receiptarray['email'] = email
    receiptarray['starttime'] = booking.StartTime + timedelta(hours=2)
    receiptarray['endtime'] = booking.EndTime + timedelta(hours=2)
    receiptarray['price'] = round(booking.PricePaid, 2)
    receiptarray['scooterID'] = booking.ScooterID
    receiptarray['duration'] = duration
    receiptarray['bookingRef'] = booking.BookingReference

    


    scooter = getScooterByBooking(booking.BookingReference)
    scooter.Available = False
    scooter.save()
    
    user = getUserByID(getIDByEmail(email))
    sendComfirmation(user.FirstName, user.LastName, email, duration, booking.StartTime, booking.EndTime, booking.PricePaid, location, booking.BookingReference)

    print("Saved!")
    return 1

def returnReceiptArray(fname,lname):
    receiptarray['fname'] = fname
    receiptarray['lname'] = lname
    return receiptarray

def getRATE():
    return RATE

def discountUser(user): # Returns multiplier for percentage discounts
    hours = getRecentTimeByUser(user).total_seconds() / 3600
    if user.DiscountStatus==0:
        discount = 1
    if user.DiscountStatus==1:
        discount = 0.9
    if user.DiscountStatus==2:
        discount = 0.8
    if hours > 8:
        discount = discount - 0.1
    return discount


def getIncomeByDay(day):
    dayDate = datetime.strptime(day, "%Y-%m-%d").date()
    income = 0
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if booking.StartTime.date() == dayDate:
            income = income + booking.PricePaid
    return income


def getIncomeWeekdays():
    #make them al 0 initially (now 5 for testing)
    incomes = [5,5,5,5,5,5,5]
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        incomes[booking.StartTime.isoweekday()-1] = incomes[booking.StartTime.isoweekday()-1] + booking.PricePaid
    return incomes


def getFeedback():
    
    text = {'loggedIn':False,
            'text1': "",
            'text2': "",
            'text3': "",
            'text4': "",
            'text5': "",
            'text6': "",
            'text7': "",
            'text8': ""
            }

    feedbacks = Feedback.objects.all()
    i = 1
    
    for feedback in feedbacks:
        st = str(i)
        sttwo = "text"
        comp = sttwo + st
        text[comp] =  "FEEDBACK: " + "\n" + feedback.Text + " RATING: " + str(feedback.Rating)
        i = i + 1
    return text 


def prioritiseFeedbacks1(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 1:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks2(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 2:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks3(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 3:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks4(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 4:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks5(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 5:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks6(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 6:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks7(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 7:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0

def prioritiseFeedbacks8(rr):
    
    if((rr == '1') or (rr =='0')):
        r = int(rr)
        rat = r
        feedbacks = Feedback.objects.all()
        for feedback in feedbacks:
            if feedback.FeedbackID == 8:
                feedback.Priority = rat
                feedback.save()
                print(feedback.Priority)
                print("Feedback1 Prioritised")
                return 1
    else:
        print("Invalid priority entered")
        return 0


def getPrioritisedFeedback():
    
    text = {'loggedIn':False,
            'Ptext1': "",
            'Ptext2': "",
            'Ptext3': "",
            'Ptext4': "",
            'Ptext5': "",
            'Ptext6': "",
            'Ptext7': "",
            'Ptext8': ""
            }

    feedbacks = Feedback.objects.all()
    i = 1
    
    for feedback in feedbacks:
        st = str(i)
        sttwo = "Ptext"
        comp = sttwo + st
        if feedback.Priority == 1:
            text[comp] = feedback.Text

        i = i + 1
    return text



def getIncomeByDuration():
    #make them all 0 initially 
    incomes = {'loggedIn':False,
                'hourly': 0,
                'fourhourly': 0,
                'daily': 0,
                'weekly': 0,
                'discount1':0,
                'discount4':0,
                'discount24':0,
                'discount168':0,
                'discountSAVED':"",
                'prediscountSAVEDpre':0,
                'preAllTime':0,
                'mon':0,
                'tue':0,
                'wed':0,
                'thur':0,
                'fri':0,
                'sat':0,
                'sun':0,
                'ALLTIMETOTAL':""
                }

    global RATE
    for user in User.objects.all():
        if user.FirstName == "Manny":
            RATE = (user.CardNum/100)
            print(RATE)

    bookings = BookingEntity.objects.all()
    for booking in bookings:
        duration = booking.EndTime - booking.StartTime
        if ((duration == timedelta(hours=1))) :
            incomes['hourly'] = incomes["hourly"] + booking.PricePaid
            if (booking.PricePaid != RATE):
                x1 = RATE - booking.PricePaid 
                incomes["discount1"] = incomes["discount1"] + x1

        elif ((duration == timedelta(hours=4))):
            incomes["fourhourly"] = incomes["fourhourly"] + booking.PricePaid
            if (booking.PricePaid != (RATE * 4)):
                x4 = (RATE * 4) - booking.PricePaid 
                incomes["discount4"] = incomes["discount4"] + x4

        elif ((duration == timedelta(days=1))):
            incomes["daily"] = incomes["daily"] + booking.PricePaid
            if (booking.PricePaid != (RATE*24)):
                x24 = (RATE * 24) - booking.PricePaid 
                incomes["discount24"] = incomes["discount24"] + x24

        elif ((duration == timedelta(weeks=1))):
            incomes["weekly"] = incomes["weekly"] + booking.PricePaid
            if (booking.PricePaid != (RATE * 168)):
                x168 = (RATE * 168) - booking.PricePaid 
                incomes["discount168"] = incomes["discount168"] + x168        
        
        incomes["prediscountSAVEDpre"] = incomes["discount1"] + incomes['discount4'] + incomes['discount24'] + incomes['discount168']
        incomes["discountSAVED"] = "{:.2f}".format(incomes["prediscountSAVEDpre"])
        incomes["preAllTime"] = incomes['hourly'] + incomes["fourhourly"] + incomes["daily"] + incomes["weekly"]
        incomes["ALLTIMETOTAL"] = "{:.2f}".format(incomes["preAllTime"])
    return incomes


def getTimeByUser(ID):
    totalTime = timedelta(hours=0)
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if booking.UserID.UserID == ID:
            duration = booking.EndTime - booking.StartTime
            totalTime = totalTime + duration
    return totalTime

def getRecentTimeByUser(ID):
    totalTime = timedelta(hours=0)
    oneMonthAgo = pytz.timezone("Europe/London").localize(datetime.now()) - timedelta(weeks=4)
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if booking.UserID == ID:
            if booking.EndTime > oneMonthAgo:
                duration = booking.EndTime - booking.StartTime
                totalTime = totalTime + duration
    return totalTime

def getRideByUser(ID): # gets soonest completing ride from user
    bookings = BookingEntity.objects.all()
    currentTime = pytz.timezone("Europe/London").localize(datetime.now())
    duration = timedelta(hours=99999)
    for booking in bookings:
        if booking.UserID.UserID == ID:
            if (booking.EndTime - currentTime) < duration and (booking.EndTime - currentTime) > timedelta(hours=0):
                duration = booking.EndTime - currentTime
    return duration


def changeScooterLocation(ID, newLocation):
    scooters = Scooter.objects.all()
    for scooter in scooters:
        if scooter.ScooterID == ID:
            scooter.Location = newLocation
            scooter.save()


def getScootersbyLocation(location):
    total = 0
    scooters = Scooter.objects.all()
    for scooter in scooters:
        if ((scooter.Location == location) and (scooter.Available == 1)):
            total = total + 1
    return total


def changeCost(newCost): #per hour
    global RATE
    RATE = newCost


def getOrderHistory(email):
    
    orders = {'loggedIn':True,
           'userEmail': email,
            'order1': '',
            'order2': '',
            'order3': '',
            'order4': '',
            'order5': '',
            'order6': '',
            'order7': '',
            'order8': '',
            'order9': '',
            'order10': ''
            }
    bookings = BookingEntity.objects.all()
    readBookings = 0
    user = getIDByEmail(email)
    for booking in bookings:
        if booking.UserID.UserID == user and readBookings<=10:
            order = "|Reference No.: " + str(booking.BookingReference)  + "| ScooterID: " + str(booking.ScooterID.ScooterID) + " |Start Time: " + str(booking.StartTime) + " |End Time: " + str(booking.EndTime) + "| Price Paid: " + str(booking.PricePaid) + "|"
            key = "order"+str(readBookings+1)
            orders[key] = order
            readBookings +=1

    return orders




#calculates avgReview rating for front page, number of all time scooters hired and all time scooter hours
def reviewRating():
    orders = {'loggedIn':False,
                'AVGrating': "",
                'noOrders':0,
                'totalhours':0,
                'hours1':0,
                'hours4':0,
                'hours24':0,
                'hours168':0,
                'x':0
                }
    feedbacks = Feedback.objects.all()
    i = 0
    
    for feedback in feedbacks:
        orders['x'] =  orders['x'] + feedback.Rating
        i = i + 1
        y = (orders['x']/i)
        orders['AVGrating'] = "{:.2f}".format(y)

    bookings = BookingEntity.objects.all()
    for booking in bookings:
        duration = booking.EndTime - booking.StartTime
        if (booking.BookingReference != 0):
            orders['noOrders'] = orders['noOrders'] + 1
        if (duration == timedelta(hours=1)):
            orders['hours1'] = orders['hours1'] + 1
        if (duration == timedelta(hours=4)):
            orders['hours4'] = orders['hours4'] + 4
        if (duration == timedelta(hours=24)):
            orders['hours24'] = orders['hours24'] + 24
        if (duration == timedelta(hours=168)):
            orders['hours168'] = orders['hours168'] + 168
    orders['totalhours'] = orders['hours1'] + orders['hours4'] + orders['hours24'] + orders['hours168']
    return orders 


def extendBooking(ref, time):
    global RATE
    for user in User.objects.all():
        if user.FirstName == "Manny":
            RATE = user.CardNum
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if int(booking.BookingReference) == int(ref):
            booking.EndTime = booking.EndTime + timedelta(hours=int(time))
            booking.PricePaid = booking.PricePaid + (int(time) * RATE)
            booking.save()

            receiptarray['endtime'] = booking.EndTime + timedelta(hours=2)
            receiptarray['price'] = round(booking.PricePaid, 2)
            receiptarray['duration'] = receiptarray['duration'] + int(time)

            user = getUserByID(booking.UserID_id)
            sendComfirmationExtend(user.FirstName, user.LastName, user.Email, 5, booking.StartTime, booking.EndTime, booking.PricePaid, time, booking.BookingReference)
            return 1
        
    return -1

def cancelBooking(ref, user):
    bookings = BookingEntity.objects.all()
    for booking in bookings:
        if int(booking.BookingReference) == int(ref):
            booking.EndTime = pytz.timezone("Europe/London").localize(datetime.now())
            booking.save()
            user = getUserByID(booking.UserID_id)
            sendComfirmationCancel(user.FirstName, user.LastName, user.Email, booking.BookingReference)
            updateScooters()
            return 1
    return -1

def sendComfirmation(name, lname, email, duration, start, end, price, location, ref):

    email_message = (
        "Hi " + name + "," + 
        "\n Thank You for using Skut! Please find booking details below:" + "\n" +
        "\n Name: " + str(name) + " " + str(lname) +
        "\n Email: " + str(email) +
        "\n Reference: " + str(ref) +
        "\n Duration: " + str(duration) +
        "\n Location: " + str(location) +
        "\n Start Time: " + str(start) +
        "\n End Time: " + str(end) +
        "\n Total Price: £" + str(round(price/100,2)) +
        "\n\n Location A =  Trinity, B = Train Station, C = Merrion, D = LGI, E = UoL Edge")
    send_mail(
        'Skut Booking Confirmation',
        email_message,
        'skutcompany@gmail.com',
        [email],
        fail_silently=True,
    )
def sendComfirmationExtend(name, lname, email, duration, start, end, price, extend, ref):
    email_message = (
        "Hi " + name + "," + 
        "\n Thank You for using Skut! Please find your extended booking details below:" + "\n" +
        "\n Name: " + str(name) + " " + str(lname) +
        "\n Email: " + str(email) +
        "\n Reference: " + str(ref) +
        "\n Duration: " + str(duration) +
        "\n Added Hour(s): " + str(extend) +
        "\n Start Time: " + str(start) +
        "\n End Time: " + str(end) +
        "\n Total Price: £" + str(round(price/100,2)) )
    send_mail(
        'Skut Extension Confirmation',
        email_message,
        'skutcompany@gmail.com',
        [email],
        fail_silently=True,
    )

def sendComfirmationCancel(name, lname, email, ref):
    email_message = (
        "Hi " + name + "," + 
        "\n This is confirmation of cancellation of the following booking:" + "\n" +
        "\n Name: " + str(name) + " " + str(lname) +
        "\n Email: " + str(email) +
        "\n Reference: " + str(ref)
    )
    send_mail(
        'Skut Extension Confirmation',
        email_message,
        'skutcompany@gmail.com',
        [email],
        fail_silently=True,
    )

def sendAccDetails(name, lname, email, password):
    email_message = (
        "Hi " + name + "," + 
        "\n Thank You for using Skut! Please find account details below:" + "\n" +
        "\n Name: " + str(name) + " " + str(lname) +
        "\n Email: " + str(email) +
        "\n Password: " + str(password) )
    send_mail(
        'Skut Account Confirmation',
        email_message,
        'skutcompany@gmail.com',
        [email],
        fail_silently=True,
    )
    





