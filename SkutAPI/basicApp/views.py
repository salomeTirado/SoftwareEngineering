import email
from inspect import currentframe
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import logout as logouts

from basicApp.models import User, BookingEntity, Feedback, Scooter
from basicApp.serializers import UserSerializer, BookingEntitySerializer, FeedbackSerializer, ScooterSerializer
from basicApp import DatabaseClass as db
from time import time, sleep
import threading

global current_user 
current_user = "none"

global RATE
for user in User.objects.all():
        if user.FirstName == "Manny":
            RATE = user.CardNum
            print(RATE)
            print("NEW RATE!!")

    



# Create your views here.


@csrf_exempt
def UserAPI(request, id=0):
    if request.method=='GET':
        users = User.objects.all()
        users_serializer = UserSerializer(users, many=True)
        return JsonResponse(users_serializer.data, safe=False)
    elif request.method=='POST':
        user_data=JSONParser().parse(request)
        user_serializer = UserSerializer(data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse("Added to database :)", safe=False)
        #return JsonResponse("Failed :(", safe=False)
    elif request.method=='PUT':
        user_data = JSONParser().parse(request)
        user=User.objects.get(UserID=user_data['UserID'])
        user_serializer=UserSerializer(user, data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return JsonResponse("Saved :)", safe=False)
        return JsonResponse("Couldn't save :(", safe=False)
    elif request.method=='DELETE':
        user=User.objects.get(UserID=id) #ID to be deleted. Sent in URL
        user.delete()
        return JsonResponse("Deleted! :)", safe=False)

@csrf_exempt
def ScooterAPI(request, id=0):
    if request.method=='GET':
        scooters = Scooter.objects.all()
        scooters_serializer = ScooterSerializer(scooters, many=True)
        return JsonResponse(scooters_serializer.data, safe=False)
    elif request.method=='POST':
        scooter_data=JSONParser().parse(request)
        scooter_serializer = ScooterSerializer(data=scooter_data)
        if scooter_serializer.is_valid():
            scooter_serializer.save()
            return JsonResponse("Added to database :)", safe=False)
        return JsonResponse("Failed :(", safe=False)
    elif request.method=='PUT':
        scooter_data = JSONParser().parse(request)
        scooter=Scooter.objects.get(ScooterID=scooter_data['ScooterID'])
        scooter_serializer=ScooterSerializer(scooter, data=scooter_data)
        if scooter_serializer.is_valid():
            scooter_serializer.save()
            return JsonResponse("Saved :)", safe=False)
        return JsonResponse("Couldn't save :(", safe=False)
    elif request.method=='DELETE':
        scooter=Scooter.objects.get(ScooterID=id) #ID to be deleted. Sent in URL
        scooter.delete()
        return JsonResponse("Deleted! :)", safe=False)

@csrf_exempt
def BookingAPI(request, id=0):
    if request.method=='GET':
        bookings = BookingEntity.objects.all()
        bookings_serializer = BookingEntitySerializer(bookings, many=True)
        return JsonResponse(bookings_serializer.data, safe=False)
    elif request.method=='POST':
        booking_data=JSONParser().parse(request)
        booking_serializer = BookingEntitySerializer(data=booking_data)
        if booking_serializer.is_valid():
            booking_serializer.save()
            return JsonResponse("Added to database :)", safe=False)
        return JsonResponse("Failed :(", safe=False)
    elif request.method=='PUT':
        booking_data = JSONParser().parse(request)
        booking=BookingEntity.objects.get(BookingID=booking_data['BookingID'])
        booking_serializer=BookingEntitySerializer(booking, data=booking_data)
        if booking_serializer.is_valid():
            booking_serializer.save()
            return JsonResponse("Saved :)", safe=False)
        return JsonResponse("Couldn't save :(", safe=False)
    elif request.method=='DELETE':
        booking=BookingEntity.objects.get(BookingID=id) #ID to be deleted. Sent in URL
        booking.delete()
        return JsonResponse("Deleted! :)", safe=False)
    

@csrf_exempt
def FeedbackAPI(request, id=0):
    if request.method=='GET':
        feedbacks = Feedback.objects.all()
        feedbacks_serializer = FeedbackSerializer(feedbacks, many=True)
        return JsonResponse(feedbacks_serializer.data, safe=False)
    elif request.method=='POST':
        feedback_data=JSONParser().parse(request)
        feedback_serializer = FeedbackSerializer(data=feedback_data)
        if feedback_serializer.is_valid():
            feedback_serializer.save()
            return JsonResponse("Added to database :)", safe=False)
        return JsonResponse("Failed :(", safe=False)
    elif request.method=='PUT':
        feedback_data = JSONParser().parse(request)
        feedback=Feedback.objects.get(FeedbackID=feedback_data['FeedbackID'])
        feedback_serializer=FeedbackSerializer(feedback, data=feedback_data)
        if feedback_serializer.is_valid():
            feedback_serializer.save()
            return JsonResponse("Saved :)", safe=False)
        return JsonResponse("Couldn't save :(", safe=False)
    elif request.method=='DELETE':
        feedback=Feedback.objects.get(FeedbackID=id) #ID to be deleted. Sent in URL
        feedback.delete()
        return JsonResponse("Deleted! :)", safe=False)


def index(request):
    AVGrating = db.reviewRating()
    return render(request, 'first.html', AVGrating)


def registerPage(request):
    return render(request, 'registration.html')


def register(request):
    userData = request.POST

    if db.register(userData):
        return render(request, 'login.html')
    else:
        return registerPage(request)

def updatepassword(request):
    userData = request.POST
    newpassword = userData.get('newpassword')

    if db.changePassword(current_user,newpassword ):
        return render(request, 'first.html')
    else:
        return registerPage(request)



def profile(request):
    user = db.getUserByID(db.getIDByEmail(current_user))
    email = user.Email
    fname = user.FirstName
    lname = user.LastName
    context = {'userEmail':email, 'userFname':fname, 'userLname': lname}
    return render(request, 'profile.html', context)


def loginPage(request, context={'loggedIn':False, 'userEmail':'none', 'errorMsg': ''}):
    return render(request, 'login.html', context)


def userLogin(request):
    global current_user
    userData = request.POST
    username = userData.get('email')
    password = userData.get('password')

    user = db.getUserByID(db.getIDByEmail(username))
    if user == -1 or user == 0:
        return loginPage(request, {'loggedIn':False, 'userEmail':'none', 'errorMsg': 'incorrect email'})
    else:
        if user.Password == password:
            if user.UserType == 1:
                return employee_booking_calendar_marchPage(request)
            elif user.UserType == 2:
                return managerstatsPage(request)
            else:
                global current_user       
                current_user = username
                
                return homePage(request, {'loggedIn':True, 'userEmail':current_user})
        else:
            return loginPage(request, {'loggedIn':False, 'userEmail':'none', 'errorMsg': 'incorrect password'})


def logout(request):
    if request.method == 'POST':
        logouts(request)
        return redirect ('first')


def homePage(request, context={'loggedIn':False, 'userEmail':current_user}): 
    return render(request, 'home.html', context)


def bookingPage(request, context={'loggedIn':False, 'userEmail':current_user}):
    
    for user in User.objects.all():
        if user.FirstName == "Manny":
            RATE = user.CardNum
            print(RATE)
            print("NEW RATE!!")

    db.updateScooters()
    global current_user
    cuser = db.getUserByID(db.getIDByEmail(current_user))
    context['userEmail'] = current_user
    context['RATE'] = RATE * 100 #removing decimal places for better Javascript calculation
    context['discount'] = db.discountUser(cuser) * 100

    if not cuser.CardName == None:
        context['CardName'] = cuser.CardName
        context['CardNum'] = cuser.CardNum
        context['CardExp'] = cuser.CardExp
    return render(request, 'booking.html', context)



def userBooking(request, context={'loggedIn':False, 'userEmail':current_user}):
   

    db.updateScooters()
    duration = 0
    bookingData = request.POST

    # organizing and assigning card details from the HTML form's format
    if not bookingData.get('cardname1') == '':
        cardname = bookingData.get('cardname1')
    if not bookingData.get('cardname3') == '':
        cardname = bookingData.get('cardname2')
    if not bookingData.get('cardname3') == '':
        cardname = bookingData.get('cardname3')
    if not bookingData.get('cardname4') == '':
        cardname = bookingData.get('cardname4')

    cardnum = 0
    if not bookingData.get('cardnum1') == '' and not bookingData.get('cardnum1') == 'None':
        cardnum = bookingData.get('cardnum1')
    if not bookingData.get('cardnum2') == '' and not bookingData.get('cardnum2') == 'None':
        cardnum = bookingData.get('cardnum2')
    if not bookingData.get('cardnum3') == '' and not bookingData.get('cardnum3') == 'None':
        cardnum = bookingData.get('cardnum3')
    if not bookingData.get('cardnum4') == '' and not bookingData.get('cardnum4') == 'None':
        cardnum = bookingData.get('cardnum4')

    cardexp = 0
    if not bookingData.get('expdate1') == '':
        cardexp = bookingData.get('expdate1')
    if not bookingData.get('expdate2') == '':
        cardexp = bookingData.get('expdate2')
    if not bookingData.get('expdate3') == '':
        cardexp = bookingData.get('expdate3')
    if not bookingData.get('expdate4') == '':
        cardexp = bookingData.get('expdate4')

    cvv = 0
    if not bookingData.get('cvv1') == '':
        cvv = bookingData.get('cvv1')
    if not bookingData.get('cvv2') == '':
        cvv = bookingData.get('cvv2')
    if not bookingData.get('cvv3') == '':
        cvv = bookingData.get('cvv3')
    if not bookingData.get('cvv4') == '':
        cvv = bookingData.get('cvv4')

    if cvv == 0 or cardexp == 0 or cardnum == 0:
        errMsg = "Your card is missing details!"
        return bookingPage(request, context={'errMsg': errMsg})
    if db.cardCheck(cardnum,cardexp,cvv) == 0:
        errMsg= "Your card details are invalid!"
        return bookingPage(request, context={'errMsg':errMsg})
    if bookingData.get('cardcheck') == 'on':
        db.saveCard(current_user, cardname, cardnum, cardexp)
    amount = int(bookingData.get('scooters'))
    duration = int(bookingData.get('dur'))
    location = bookingData.get('location')


    for i in range(0,amount): # makes bookings based on amount of scooters booked
        if db.makeBooking(current_user,duration,location) == 0:
            errMsg= "Booking #" + str(i+1) + " failed! There are no more available scooters for this location!"
            return bookingPage(request, context={'errMsg':errMsg})

    rideTime = db.getRideByUser(db.getIDByEmail(current_user)).total_seconds()
    context['secondsLeft'] = int(rideTime)
    return render(request, 'ride.html', context)


def feedback(request, context={'loggedIn':False, 'userEmail':current_user}):

    feedbackData = request.POST
    rating = feedbackData.get('rating')
    text = feedbackData.get('comment')
    bookRef = feedbackData.get('bookRef')
    email = feedbackData.get('email')

    feedback = db.leaveFeedback(email,rating,text,bookRef)
    if feedback == 1:
        context["submission"] = "Your feedback has been submitted!"
    elif feedback == 0:
        context["submission"] = "Invalid booking reference!"
    elif feedback == -1:
        context["submission"] = "Invalid email!"
    return render(request, 'about.html', context)


def termsconditionsPage(request, context={'loggedIn':False}): 
    return render(request, 'termsconditions.html', context)

def aboutF(request): 
    return render(request, 'aboutf.html') 

def safetyPage(request, context={'loggedIn':False}): 
    return render(request, 'safety.html', context) 


def initialSafetyPage(request, context={'loggedIn':False}): 
    return render(request, 'initialSafety.html', context) 


def ridePage(request, context={'loggedIn':False}): 
    rideTime = db.getRideByUser(db.getIDByEmail(current_user)).total_seconds()
    context['secondsLeft'] = int(rideTime)
    return render(request, 'ride.html', context)

def extendPage(request, context={'loggedIn':False}): 
    return render(request, 'extend.html', context)

def userExtend(request, context={'loggedIn':False}): 
    print("extend")
    bookingData = request.POST
    reference = bookingData.get('ref')
    time = bookingData.get('time')
    extension = db.extendBooking(reference, time)
    if extension == 1:
        print("success")
        rideTime = db.getRideByUser(db.getIDByEmail(current_user)).total_seconds()
        context['secondsLeft'] = int(rideTime)
        return render(request, 'ride.html', context)
    else:
        print("failed")
        return render(request, 'extend.html', context)
    
def cancelPage(request, context={'loggedIn':False, 'userEmail':current_user}): 
    return render(request, 'cancel.html', context)

def userCancel(request, context={'loggedIn':False, 'userEmail':current_user}): 
    print("cancel")
    bookingData = request.POST
    reference = bookingData.get('ref')
    cancel = db.cancelBooking(reference, current_user)
    if cancel == 1:
        print("success")
        return homePage(request, context)
    else:
        print("failed - booking not found")
        return render(request, 'cancel.html', context)

def aboutPage(request, context={'loggedIn':False}): 
    return render(request, 'about.html', context)


def employee_booking_calendar_marchPage(request, context={'loggedIn':False}):
     
    return render(request, 'employee_booking_calendar_march.html', context)

def empregister(request):
    userData = request.POST

    if db.register(userData):
        return render(request, 'login.html')
    else:
        return registerPage(request) #TODO: ADD MESSAGE FOR FAILED REGISTRATION


def employee_booking_calendar_aprilPage(request, context={'loggedIn':False}): 
    return render(request, 'employee_booking_calendar_april.html', context)


def employee_booking_calendar_februaryPage(request, context={'loggedIn':False}): 
    userData = request.POST
    newpassword = userData.get('newpassword')

    if db.changePassword(current_user,newpassword ):
        return render(request, 'employee_booking_calendar_february.html')


def employee_bookingPage(request, context={'loggedIn':False}): 
    return render(request, 'employee_booking.html', context)

def employeeprofile(request):
    return render(request, 'employeeprofile.html')     


def managerstatsPage(request): 
    for user in User.objects.all():
        if user.FirstName == "Manny":
            if user.CardNum == None:
                user.CardNum = 230
                user.save()
        
    incomes = db.getIncomeByDuration()
    dayIncomes = db.getIncomeWeekdays()
    incomes["mon"] = dayIncomes[0]
    incomes["tue"] = dayIncomes[1]
    incomes["wed"] = dayIncomes[2]
    incomes["thur"] = dayIncomes[3]
    incomes["fri"] = dayIncomes[4]
    incomes["sat"] = dayIncomes[5]
    incomes["sun"] = dayIncomes[6]
    return render(request, 'managerstats.html', incomes)


def managerprofile(request):
    return render(request, 'managerprofile.html')  


def managerconfigPage(request, context={'loggedIn':False}): 
    return render(request, 'managerconfig.html', context)

def managerconfigFIX(request):

    
    print("WORKING")
    for user in User.objects.all():
        if user.FirstName == "Manny":
            if user.CardNum == None:
                user.CardNum = 230
                user.save()
            else:
                user.save()
                hourData = request.POST
                newrate = hourData.get('hour')
                user.CardNum = newrate
                user.save()
                print(RATE)
                print("NEW RATE!!")
        
    incomes = db.getIncomeByDuration()
    dayIncomes = db.getIncomeWeekdays()
    incomes["mon"] = dayIncomes[0]
    incomes["tue"] = dayIncomes[1]
    incomes["wed"] = dayIncomes[2]
    incomes["thur"] = dayIncomes[3]
    incomes["fri"] = dayIncomes[4]
    incomes["sat"] = dayIncomes[5]
    incomes["sun"] = dayIncomes[6]
    return render(request, 'managerstats.html', incomes)
    



def scooterListPage(request):
    return render(request, 'scooterlist.html')



def hospitalTrackPage(request):
    totalHospital = db.getScootersbyLocation("D")
    context={'total':totalHospital}
    return render(request, 'HospitalTrack.html',context)


def merrionTrackPage(request):
    totalMerrion = db.getScootersbyLocation("C")
    context={'total':totalMerrion}
    return render(request, 'MerrionTrack.html',context)


def trainTrackPage(request):
    totalTrain = db.getScootersbyLocation("B")
    context={'total':totalTrain}
    return render(request, 'TrainTrack.html',context)




def trinityTrackPage(request):
    totalTrinity = db.getScootersbyLocation("A")
    context={'total':totalTrinity}
    return render(request, 'TrinityTrack.html',context)

def edgeTrackPage(request):
    totalEdge = db.getScootersbyLocation("E")
    context={'total':totalEdge}
    return render(request, 'EdgeTrack.html',context)


def updateScooterLocations(request):
    scooters = Scooter.objects.all()
    for scooter in scooters:
        workingID = str(scooter.ScooterID)
        newLoc = request.POST.get(workingID)
        if newLoc in ['A', 'B', 'C', 'D', 'E', 'F','G']:
            scooter.Location = newLoc
            scooter.save()
    return managerconfigPage(request)
 

def updateCost(request):
    newCost = float(request.POST.get("hour"))
    db.changeCost(newCost)
    return managerconfigPage(request)
    

def RankFeedbackPage(request):
    db.getFeedback()
    userData = request.POST
    rating1 = userData.get('feedbacks1')
    rating2 = userData.get('feedbacks2')
    rating3 = userData.get('feedbacks3')
    rating4 = userData.get('feedbacks4')
    rating5 = userData.get('feedbacks5')
    rating6 = userData.get('feedbacks6')
    rating7 = userData.get('feedbacks7')
    rating8 = userData.get('feedbacks8')
    db.prioritiseFeedbacks1(rating1)
    db.prioritiseFeedbacks2(rating2)
    db.prioritiseFeedbacks3(rating3)
    db.prioritiseFeedbacks4(rating4)
    db.prioritiseFeedbacks5(rating5)
    db.prioritiseFeedbacks6(rating6)
    db.prioritiseFeedbacks7(rating7)
    db.prioritiseFeedbacks8(rating8)

    return render(request, 'RankFeedback.html',db.getFeedback()) 




def ViewFeedbackPage(request): 
    db.getPrioritisedFeedback()
    return render(request, 'ViewFeedback.html', db.getPrioritisedFeedback())   

def receiptPage(request):
    print("printed receipt")

    user = db.getUserByID(db.getIDByEmail(current_user))
    fname = user.FirstName
    lname = user.LastName
    return render(request, 'receipt.html', db.returnReceiptArray(fname,lname)) 

def orderhistory(request):
    orderHistory = db.getOrderHistory(current_user)
    return render(request, 'orderhistory.html',orderHistory)


