from tkinter import CASCADE
from django.db import models

# Create your models here.
class User(models.Model):
    UserID = models.AutoField(primary_key=True)
    FirstName = models.CharField(max_length=100)
    LastName = models.CharField(max_length=100)
    Email = models.CharField(max_length=100)
    Password = models.CharField(max_length=100)
    UserType = models.IntegerField()
    DiscountStatus = models.IntegerField()
    CardName = models.CharField(max_length=100)
    CardNum = models.IntegerField()
    CardExp = models.CharField(max_length=100)


class Scooter(models.Model):
    ScooterID = models.AutoField(primary_key=True)
    Location = models.CharField(max_length=1)
    Available = models.BooleanField()

class BookingEntity(models.Model):
    BookingReference = models.AutoField(primary_key=True)
    UserID = models.ForeignKey(User, on_delete=models.CASCADE)
    ScooterID = models.ForeignKey(Scooter, on_delete=models.CASCADE)
    StartTime = models.DateTimeField()
    EndTime = models.DateTimeField()
    PricePaid = models.FloatField()

class Feedback(models.Model):
    FeedbackID = models.AutoField(primary_key=True)
    UserID = models.ForeignKey(User, on_delete=models.CASCADE)
    BookingReference = models.ForeignKey(BookingEntity, on_delete=models.CASCADE)
    Rating = models.IntegerField()
    Text = models.CharField(max_length=500)
    Priority = models.IntegerField()