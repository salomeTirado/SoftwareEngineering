from xml.etree.ElementInclude import include
#from django.conf.urls import url
from basicApp import views
from django.urls import path, re_path


urlpatterns=[
    
    path('', views.index, name='first'),
    re_path(r'^login/userLogin', views.userLogin),
    re_path(r'^userLogin', views.userLogin, name='userLogin'),
    re_path(r'^logout', views.logout, name='logout'),
    re_path(r'^login/bookingPage', views.bookingPage, name='bookingPage'),
    re_path(r'^bookingPage', views.bookingPage, name='bookingPage'),
    re_path(r'^userBooking', views.userBooking, name='userBooking'),
    re_path(r'^login/userBooking', views.userBooking, name='userBooking'),
    re_path(r'^managerconfig/updateScooterLocations', views.updateScooterLocations, name='updateScooterLocations'),
    re_path(r'^updateScooterLocations', views.updateScooterLocations, name='updateScooterLocations'),
    re_path(r'^first', views.index),
    re_path(r'^registration', views.registerPage),
    re_path(r'^orderhistory', views.orderhistory),
    re_path(r'^register', views.register),
     re_path(r'^updatepassword', views.updatepassword),
    re_path(r'^loginPage', views.loginPage),
    re_path(r'^login', views.loginPage),
    re_path(r'^profile', views.profile),
    re_path(r'^managerprofile', views.managerprofile),
    re_path(r'^home', views.homePage),
    re_path(r'^booking', views.bookingPage),
    re_path(r'^termsConditions', views.termsconditionsPage),
    re_path(r'^safety', views.safetyPage),
    re_path(r'^initialSafety', views.initialSafetyPage),
    re_path(r'^aboutF', views.aboutF),
    re_path(r'^ride', views.ridePage),
    re_path(r'^about', views.aboutPage),
    re_path(r'^feedback', views.feedback),
    re_path(r'^receipt', views.receiptPage),
    re_path(r'^employee_booking_calendar_march', views.employee_booking_calendar_marchPage),
    re_path(r'^empregister', views.empregister),
    re_path(r'^employee_booking_calendar_april', views.employee_booking_calendar_aprilPage),
    re_path(r'^employee_booking_calendar_february', views.employee_booking_calendar_februaryPage),
    re_path(r'^employee_booking', views.employee_bookingPage),
    re_path(r'^employeeprofile', views.employeeprofile),
    re_path(r'^managerstats', views.managerstatsPage),
    re_path(r'^managerconfig', views.managerconfigPage),
    re_path(r'scooterList', views.scooterListPage),
    re_path(r'EdgeTrack', views.edgeTrackPage),
    re_path(r'HospitalTrack', views.hospitalTrackPage),
    re_path(r'MerrionTrack', views.merrionTrackPage),
    re_path(r'TrainTrack', views.trainTrackPage),
    re_path(r'TrinityTrack', views.trinityTrackPage),
    re_path(r'RankFeedback', views.RankFeedbackPage),
    re_path(r'ViewFeedback', views.ViewFeedbackPage),
    re_path(r'extend', views.extendPage),
    re_path(r'userExtend', views.userExtend),
    re_path(r'cancel', views.cancelPage),
    re_path(r'userCancel', views.userCancel),
    re_path(r'FIXmanagerconfigFIX', views.managerconfigFIX),
    
    
    

    re_path(r'^viewuser/$', views.UserAPI),
    re_path(r'^viewuser/([0-99]+)$', views.UserAPI),
    re_path(r'^viewscooter/$', views.ScooterAPI),
    re_path(r'^viewscooter/([0-99]+)$', views.ScooterAPI),
    re_path(r'^viewbooking/$', views.BookingAPI),
    re_path(r'^viewbooking/([0-99]+)$', views.BookingAPI),
    re_path(r'^viewfeedback/$', views.FeedbackAPI),
    re_path(r'^viewfeedback/([0-99]+)$', views.FeedbackAPI)

    
]
