# SoftwareEngineering
How to clone:
```
cd existing_repo
git remote add origin https://gitlab.com/salomeTirado/SoftwareEngineering.git
git branch -M main
git push -uf origin main
```
## How to run:
Step 1: make sure you have python installed. Python3 is what's worked so far
Step 2: pip install the necessary packages using pip install django-cors-headers and pip install djangorestframework
Step 3: Navigate to SkutAPI (the one with manage.py in it) in the terminal
Step 4: python3 manage.py runserver
Step 5: The terminal will show you an IP address (your home address). Go to that IP address in an internet browser. You should see the webpages we've created.


## Name
Team 37 Software Engineering Project

## Description
A web application called Sküt that allows users to book e-scooters around Leeds. It has multiple levels of authorisation for users, employees and managers
