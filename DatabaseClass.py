#login - should check login credentials against database
#register -should add values to database

#Functions:
#Connect
#login
#register

users = {"user1": "pass1", "user2": "pass2", "user3": "pass3"}
string1 = ""

# testing if username exists
def userTest(string, users):
    if string in users:
        global string1
        string1 = string
        return 1
    else:
        print("username does not exist")
        return 0

# testing if pass matches username
def passTest(string, users):

    if string1 == "": # debug case, shouldn't happen to users if username gets validated first in code
        print("MAKE SURE USER IS VALIDATED FIRST")
        return 2
    if string in users[string1]:
        return 1
    else:
        print("incorrect password")
        return 0

# registration with new username and pass
def regUserTest(string, passString, users):

    symbols = ['$', '@', '#', '%', '!', '£']

    # check if user is taken
    if string in users:
        print("username taken")
        return 0
    else:
        ret = 1

    # check if password is valid
    if len(passString) < 6:
        print('password cannot be less than 6 characters')
        ret = 0

    if len(passString) > 20:
        print('password cannot be longer than 20 characters')
        ret = 0

    if not any(char.isdigit() for char in passString):
        print('password should have at least one number')
        ret = 0

    if not any(char.isupper() for char in passString):
        print('password should have at least one uppercase letter')
        ret = 0

    if not any(char.islower() for char in passString):
        print('password should have at least one lowercase letter')
        ret = 0

    if not any(char in symbols for char in passString):
        print('password should have at least one symbol')
        ret = 0
    if ret == 0:
        return ret

    # appends valid username/pass to dict
    else:
        users[string] = {string: passString}
        return ret



def connect():
    print("connect to database")

def login(username,password):
    print("login")
    if userTest(username, users):
        passTest(password, users)
    
def register(firstName,surname,email,username,password,userType):
    print("register")
    regUserTest(username, password, users)

login("user2","pass2")
login("user2", "Pass2")
register("","","","user3","Ppass!3","")
register("","","","user4","Ppass44","")
register("","","","user4","Ppass!4","")
print(users["user4"])
login("user4","Ppass!4")

# TODO: Find out why new pass Ppass!4 is incorrect when registered
# TODO: Major rework to accomodate SQL and frontend